package ee.kristian.hunt.flowerstore.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderDetailRepository orderDetailRepository;

    public Page<CustomerOrder> getAll(Pageable pageable) {
        log.info("Getting a page of all orders");
        return orderRepository.findAll(pageable);
    }

    public OrderDetail saveOrderDetail(OrderDetail orderDetail) {
        return orderDetailRepository.save(orderDetail);
    }

    public CustomerOrder save(CustomerOrder order) {
        return orderRepository.save(order);
    }
}
