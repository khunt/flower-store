
# FLowerstore

## Getting started
starting the services: 
```
docker-compose up -d
```

access to Swagger UI (API documentation and testing web interface):
```
http://localhost:8080/swagger-ui.html
```

stopping the services:
```
docker-compose stop
```

## Generating mockdata
Before execution make sure services are started.
Generate mockdata with the following command inside the flowerstore-backend container:
```
java -jar flowerstore.jar mockdata --server.port=9090
# for example:
docker exec -it flower-store_flowerstore-backend_1 java -jar flowerstore.jar mockdata --server.port=9090
```

## Additional information
For reference documentation please check HELP.md.
