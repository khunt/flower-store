package ee.kristian.hunt.flowerstore;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import ee.kristian.hunt.flowerstore.mockdata.MockDataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class FlowerstoreApplication implements CommandLineRunner {

    private final MockDataService mockDataService;

    private final ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(FlowerstoreApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if (args.length > 0) {
            for (String arg : args) {
                if (arg.equals("mockdata")) {
                    log.info("Generating mock data");
                    mockDataService.generate();
                    log.info("Mock data generation is complete. Shutting down.");
                    SpringApplication.exit(context, () -> 0);
                }
            }
        }
    }
}
