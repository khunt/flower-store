FROM openjdk:11-jdk-slim as builder

WORKDIR /tmp
COPY gradle /tmp/gradle
COPY build.gradle settings.gradle gradlew /tmp/
RUN ./gradlew build || return 0
COPY src /tmp/src
RUN ./gradlew build

FROM openjdk:11-jre-slim
WORKDIR /srv/app
COPY --from=builder /tmp/build/libs/*.jar ./flowerstore.jar
EXPOSE 8080
CMD ["java", "-jar", "flowerstore.jar"]
