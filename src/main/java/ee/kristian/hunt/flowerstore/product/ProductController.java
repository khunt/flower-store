package ee.kristian.hunt.flowerstore.product;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.kristian.hunt.flowerstore.ApiPageable;
import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("/")
    @ApiPageable
    public Page<Product> getAll(@ApiIgnore Pageable pageable) {
        return productService.getAll(pageable);
    }

    @GetMapping("/related/{id}")
    public List<Product> findRelatedProducts(@PathVariable Long id) {
        return productService.findRelated(id);
    }
}
