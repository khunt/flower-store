package ee.kristian.hunt.flowerstore.product;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ee.kristian.hunt.flowerstore.order.CustomerOrder;
import ee.kristian.hunt.flowerstore.order.OrderDetail;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @InjectMocks
    private ProductService service;

    @Mock
    private ProductRepository repository;

    private Product productA;

    private Product productB;

    private Product productC;

    @BeforeEach
    public void setUp() {
        productA = Product.builder().id(1L).name("A").build();
        productB = Product.builder().id(2L).name("B").build();
        productC = Product.builder().id(3L).name("C").build();
        CustomerOrder order = new CustomerOrder();
        OrderDetail orderDetail11 = createOrderDetail(productA, order);
        OrderDetail orderDetail12 = createOrderDetail(productB, order);
        OrderDetail orderDetail13 = createOrderDetail(productC, order);
        OrderDetail orderDetail14 = createOrderDetail(productC, order);
        order.setOrderDetails(List.of(orderDetail11, orderDetail12, orderDetail13, orderDetail14));
    }

    @Test
    public void findRelatedOnNotExistingProductThrowsException() {
        Long notExistingProductId = 999L;
        when(repository.findById(notExistingProductId)).thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> service.findRelated(notExistingProductId));
    }

    @Test
    public void findRelatedProductDoesNotIncludeSelf() {
        Long existingProductId = 1L;
        when(repository.findById(existingProductId)).thenReturn(Optional.of(productA));
        when(repository.findAll()).thenReturn(List.of(productA, productC));

        List<Product> related = service.findRelated(existingProductId);

        assertThat(related).doesNotContain(productA);
    }

    @Test
    public void findRelatedProductDoesIsReturnedInCorrectOrder() {
        Long existingProductId = 1L;
        when(repository.findById(existingProductId)).thenReturn(Optional.of(productA));
        when(repository.findAll()).thenReturn(List.of(productA, productB, productC));

        List<Product> related = service.findRelated(existingProductId);

        assertThat(related.get(0)).isEqualTo(productC);
    }

    @Test
    public void findRelatedProductDoesNotIncludeProductWhichHasNotBeenPurchasedTogether() {
        Long existingProductId = 1L;
        Product productD = Product.builder().id(4L).name("D").build();
        when(repository.findById(existingProductId)).thenReturn(Optional.of(productA));
        when(repository.findAll()).thenReturn(List.of(productA, productB, productC, productD));

        List<Product> related = service.findRelated(existingProductId);

        assertThat(related).doesNotContain(productD);
    }

    private OrderDetail createOrderDetail(Product product, CustomerOrder order) {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setCustomerOrder(order);
        orderDetail.setProduct(product);
        List<OrderDetail> orderDetails = product.getOrderDetails() != null ? product.getOrderDetails() : new ArrayList<>();
        orderDetails.add(orderDetail);
        product.setOrderDetails(orderDetails);
        return orderDetail;
    }
}