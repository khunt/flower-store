package ee.kristian.hunt.flowerstore.product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ee.kristian.hunt.flowerstore.order.CustomerOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository repository;

    public Page<Product> getAll(Pageable pageable) {
        log.info("Getting a page of all products");
        return repository.findAll(pageable);
    }

    public List<Product> findRelated(Long id) {
        log.info("Finding related products for product with id: {}", id);
        Product product = repository.findById(id).orElseThrow(ProductNotFoundException::new);
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .filter(other -> !product.equals(other))
                .filter(other -> hasBeenPurchasedTogether(product, other))
                .sorted(Comparator.comparing(relatedProduct -> relatedProduct.getOrderDetails().size(), Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    public Product save(Product newProduct) {
        return repository.save(newProduct);
    }

    private boolean hasBeenPurchasedTogether(Product product, Product other) {
        if (product.getOrderDetails() == null) {
            return false;
        }

        for (var productOrderDetails : product.getOrderDetails()) {
            CustomerOrder order = productOrderDetails.getCustomerOrder();
            if (orderContainsProduct(other, order)) {
                return true;
            }
        }
        return false;
    }

    private boolean orderContainsProduct(Product other, CustomerOrder order) {
        if (order != null) {
            return order.getOrderDetails().stream()
                    .anyMatch(orderDetail -> orderDetail.getProduct().equals(other));
        }
        return false;
    }
}
