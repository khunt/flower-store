package ee.kristian.hunt.flowerstore;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PriceUtil {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bigDecimalValue = BigDecimal.valueOf(value);
        bigDecimalValue = bigDecimalValue.setScale(places, RoundingMode.HALF_UP);
        return bigDecimalValue.doubleValue();
    }
}
