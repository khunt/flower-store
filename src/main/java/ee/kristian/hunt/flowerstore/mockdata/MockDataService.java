package ee.kristian.hunt.flowerstore.mockdata;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;

import ee.kristian.hunt.flowerstore.order.CustomerOrder;
import ee.kristian.hunt.flowerstore.order.OrderDetail;
import ee.kristian.hunt.flowerstore.order.OrderService;
import ee.kristian.hunt.flowerstore.product.Product;
import ee.kristian.hunt.flowerstore.product.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class MockDataService {
    private final OrderService orderService;
    private final ProductService productService;

    public void generate() {
        List<Product> products = generateProducts();
        generateOrders(products);
    }

    private void generateOrders(List<Product> products) {
        log.info("Generating mock data orders");
        for (int i = 0; i < 50; i++) {
            var order = orderService.save(new CustomerOrder());
            order.setCreated(generateCreated());
            order.setOrderDetails(generateOrderDetails(products, order));
            order.setTotal();
            orderService.save(order);
        }
    }

    private List<Product> generateProducts() {
        log.info("Generating mock data products");
        List<String> productNames = List.of("Tulip", "Daffodil", "Poppy", "Sunflower", "Bluebell", "Rose", "Snowdrop",
                "Cherry blossom", "Orchid", "Iris", "Peony", "Chrysanthemum", "Geranium", "Lily", "Lotus",
                "Water lily", "Dandelion", "Hyacinth", "Daisy", "Crocus");
        List<Product> persistedProducts = new ArrayList<>();
        for (var productName : productNames) {
            var newProduct = Product.builder()
                    .name(productName)
                    .price(ThreadLocalRandom.current().nextInt(100, 1999) / 100d)
                    .stock(ThreadLocalRandom.current().nextInt(1, 99))
                    .build();
            persistedProducts.add(productService.save(newProduct));
        }
        return persistedProducts;
    }

    private List<OrderDetail> generateOrderDetails(List<Product> products, CustomerOrder order) {
        int numberOfOrderDetails = ThreadLocalRandom.current().nextInt(1, 5);
        List<Integer> productIndicesInOrder = new ArrayList<>();
        List<OrderDetail> orderDetails = new ArrayList<>();
        for (int i = 0; i < numberOfOrderDetails; i++) {
            Product productBeingOrdered = findProductNotInOrder(products, productIndicesInOrder);
            OrderDetail detail = new OrderDetail();
            detail.setProduct(productBeingOrdered);
            detail.setQuantity(ThreadLocalRandom.current().nextInt(1, 10));
            detail.setSubtotal();
            detail.setCustomerOrder(order);
            orderDetails.add(orderService.saveOrderDetail(detail));
        }
        return orderDetails;
    }

    private Product findProductNotInOrder(List<Product> products, List<Integer> productIndicesInOrder) {
        Product productBeingOrdered = null;
        while (productBeingOrdered == null) {
            int productIndex = ThreadLocalRandom.current().nextInt(products.size());
            if (!productIndicesInOrder.contains(productIndex)) {
                productIndicesInOrder.add(productIndex);
                productBeingOrdered = products.get(productIndex);
            }
        }
        return productBeingOrdered;
    }

    private LocalDateTime generateCreated() {
        var zone = ZoneOffset.of("Z");
        long minDateTime = LocalDateTime.now().minusDays(365).toEpochSecond(zone);
        long maxDateTime = LocalDateTime.now().toEpochSecond(zone);
        return LocalDateTime.ofEpochSecond(ThreadLocalRandom.current().nextLong(minDateTime, maxDateTime), 0, zone);
    }

}
