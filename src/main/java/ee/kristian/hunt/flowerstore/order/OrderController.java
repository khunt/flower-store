package ee.kristian.hunt.flowerstore.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.kristian.hunt.flowerstore.ApiPageable;
import lombok.RequiredArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/")
    @ApiPageable
    public Page<CustomerOrder> getAll(@ApiIgnore Pageable pageable) {
        return orderService.getAll(pageable);
    }
}
