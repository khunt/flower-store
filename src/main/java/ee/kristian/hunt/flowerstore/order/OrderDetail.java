package ee.kristian.hunt.flowerstore.order;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ee.kristian.hunt.flowerstore.PriceUtil;
import ee.kristian.hunt.flowerstore.product.Product;
import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_order_id")
    @JsonIgnore
    @ToString.Exclude
    private CustomerOrder customerOrder;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @JsonIgnore
    @ToString.Exclude
    private Product product;

    private Integer quantity;

    private Double subtotal;

    public String getProductName() {
        return product != null ? product.getName() : "";
    }

    public Long getProductId() {
        return product != null ? product.getId() : 0L;
    }

    public void setSubtotal() {
        double calculatedValue = this.getProduct().getPrice() * (double) this.quantity;
        this.subtotal = PriceUtil.round(calculatedValue, 2);
    }
}
