package ee.kristian.hunt.flowerstore.order;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import ee.kristian.hunt.flowerstore.PriceUtil;
import lombok.Data;

@Entity
@Data
public class CustomerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private LocalDateTime created;

    private Double total;

    @OneToMany(mappedBy = "customerOrder")
    private List<OrderDetail> orderDetails;

    public void setTotal() {
        double calculatedTotal = this.getOrderDetails().stream()
                .map(OrderDetail::getSubtotal)
                .reduce(Double::sum)
                .orElse(0d);
        this.total = PriceUtil.round(calculatedTotal, 2);
    }
}
