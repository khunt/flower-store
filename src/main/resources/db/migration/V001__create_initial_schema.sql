create sequence hibernate_sequence;

create table product
(
    id    bigint not null
        constraint product_pkey
            primary key,
    name  varchar(255),
    price double precision,
    stock integer
);

create table customer_order
(
    id      bigint not null
        constraint customer_order_pkey
            primary key,
    created timestamp,
    total   double precision
);

create table order_detail
(
    id                bigint not null
        constraint order_detail_pkey
            primary key,
    quantity          integer,
    subtotal          double precision,
    customer_order_id bigint
        constraint order_detail_customer_order_fkey
            references customer_order,
    product_id        bigint
        constraint order_detail_product_fkey
            references product
);
