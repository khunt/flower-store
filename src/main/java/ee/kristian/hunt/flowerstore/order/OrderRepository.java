package ee.kristian.hunt.flowerstore.order;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<CustomerOrder, Long> {
}
